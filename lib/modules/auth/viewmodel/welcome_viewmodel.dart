import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class WelcomeViewModel {
  Future<void> login(BuildContext context) async {
    Navigator.pushNamed(context, 'login');
  }

  Future<void> register(BuildContext context) async {
    Navigator.pushNamed(context, 'register');
  }
}
