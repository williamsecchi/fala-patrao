import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../../../core/enum/type_user.dart';
import '../../../core/interfaces/auth_interface.dart';
import '../../../core/model/success_error.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/register_model.dart';

class RegisterViewModel extends FirebaseOperationsImplementation {
  final RegisterModel _model = RegisterModel();

  final IAuthSerice iAuthSerice;

  RegisterViewModel(this.iAuthSerice);

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      _model.scaffoldMessengerKey;

  GlobalKey<FormState> get formKey => _model.formKey;

  TextEditingController get nameController => _model.nameController;

  TextEditingController get emailController => _model.emailController;

  TextEditingController get passwordController => _model.passwordController;

  TextEditingController get passwordConfirmationController =>
      _model.passwordConfirmationController;

  TypeUser get typeUser => _model.typeUser;

  List<TypeUser> get typeUsers => TypeUser.values;

  void setTypeUser(TypeUser typeUser) {
    _model.typeUser = typeUser;
  }

  void register() async {
    FocusScope.of(scaffoldMessengerKey.currentContext!).unfocus();

    if (!formKey.currentState!.validate()) {
      return;
    }

    final SuccessError userCreated = await iAuthSerice.register(
      emailController.text.trim(),
      passwordController.text.trim(),
    );

    if (userCreated.isSuccess) {
      if (userCreated.value != null) {
        final bool ableToLogUseIn = await iAuthSerice.login(
          emailController.text.trim(),
          passwordController.text.trim(),
        );

        if (ableToLogUseIn) {
          final User? user = await iAuthSerice.getUser();
          user?.updateDisplayName(
            nameController.text.trim(),
          );

          if (user != null) {
            Map<String, dynamic> userMap = <String, dynamic>{
              'name': nameController.text.trim(),
              'email': emailController.text.trim(),
              'type': typeUser.index,
              'date': DateTime.now().millisecondsSinceEpoch,
            };
            setDocument(
                FirebaseDatabase.instance.ref('usuarios/${user.uid}'), userMap);

            user.sendEmailVerification();
            scaffoldMessengerKey.currentState!.showSnackBar(
              const SnackBar(
                content: Text(
                  'Cadastro realizado com sucesso! Por favor abra seu e-mail e confirme seu cadastro clicando no link enviado! (verifique a pasta de spam)',
                  style: TextStyle(color: Colors.white),
                ),
                backgroundColor: Colors.green,
                duration: Duration(
                  seconds: 10,
                ),
              ),
            );
          }
        }
      }
    } else {
      if (userCreated.value is FirebaseAuthException) {
        final FirebaseAuthException authException =
            userCreated.value as FirebaseAuthException;

        if (authException.code == 'email-already-in-use') {
          scaffoldMessengerKey.currentState!.showSnackBar(
            const SnackBar(
              content: Text(
                'E-mail já cadastrado!',
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Colors.red,
              duration: Duration(
                seconds: 10,
              ),
            ),
          );
        } else {
          scaffoldMessengerKey.currentState!.showSnackBar(
            const SnackBar(
              content: Text(
                'Erro ao cadastrar usuário!',
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Colors.red,
              duration: Duration(
                seconds: 10,
              ),
            ),
          );
        }
      } else {
        scaffoldMessengerKey.currentState!.showSnackBar(
          const SnackBar(
            content: Text('Erro ao criar usuário!'),
          ),
        );
      }
    }
  }
}
