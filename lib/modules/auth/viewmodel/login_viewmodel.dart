import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/interfaces/app_service.dart';
import '../../../core/interfaces/auth_interface.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/login_model.dart';

class LoginViewModel extends FirebaseOperationsImplementation {
  final LoginModel _model = LoginModel();

  final IAuthSerice _authSerice;
  final IAppService _appService;

  LoginViewModel(this._authSerice, this._appService);

  TextEditingController get emailController => _model.emailController;

  TextEditingController get passwordController => _model.passwordController;

  GlobalKey<FormState> get formKey => _model.formKey;

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      _model.scaffoldMessengerKey;

  Future<void> login(BuildContext context) async {
    if (!_model.formKey.currentState!.validate()) {
      scaffoldMessengerKey.currentState!.showSnackBar(
        const SnackBar(
          content: Text('Por favor, verifique os dados informados!'),
        ),
      );
      return;
    }

    final bool authSuccess = await _authSerice.login(
      _model.emailController.text.trim(),
      _model.passwordController.text.trim(),
    );

    if (authSuccess) {
      if (await _authSerice.isEmailVerified()) {
        User? user = await _authSerice.getUser();

        if (user != null) {
          final bool isTypeUserPresent = await _appService.getAndSetTypeUser(user.uid);

          if (isTypeUserPresent) {
            Modular.to.pushReplacementNamed('/home/');
          } else {
            showSnackBar();
          }
        } else {
          showSnackBar();
        }
      } else {
        scaffoldMessengerKey.currentState!.showSnackBar(
          SnackBar(
            content: const Text(
              'Por favor, confirme o seu cadastro clicando no link enviado para o seu e-mail. (verifique a pasta spam)',
            ),
            duration: Duration(seconds: 10),
            action: SnackBarAction(
              label: 'Reenviar e-mail',
              onPressed: () => _authSerice.resentEmailVerification(),
            ),
          ),
        );
      }
    } else {
      scaffoldMessengerKey.currentState!.showSnackBar(
        const SnackBar(
          content: Text('E-mail ou senha informados não conferem!'),
        ),
      );
    }
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showSnackBar() {
    return scaffoldMessengerKey.currentState!.showSnackBar(
      const SnackBar(
        content: Text('Erro no login! Tente novamente mais tarde.'),
      ),
    );
  }

  void register(BuildContext context) {
    Navigator.pushNamed(context, '/auth/register');
  }
}
