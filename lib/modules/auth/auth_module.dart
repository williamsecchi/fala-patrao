import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

import '../../core/guard/auth_guard.dart';
import '../home/home_module.dart';
import 'view/login_view.dart';
import 'view/register_view.dart';
import 'view/welcome_view.dart';
import 'viewmodel/login_viewmodel.dart';
import 'viewmodel/register_viewmodel.dart';
import 'viewmodel/welcome_viewmodel.dart';

class AuthModule extends Module {
  @override
  List<Bind<Object>> get binds => <Bind<Object>>[
        Bind<LoginViewModel>(
          (Injector<dynamic> i) => LoginViewModel(
            i.get(),
            i.get(),
          ),
        ),
        Bind<WelcomeViewModel>(
          (Injector<dynamic> i) => WelcomeViewModel(),
        ),
        Bind<RegisterViewModel>(
          (Injector<dynamic> i) => RegisterViewModel(
            i.get(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute<ModularRoute>(
          '/',
          child: (BuildContext context, ModularArguments args) {
            return const WelcomeView();
          },
          transition: TransitionType.fadeIn,
        ),
        ChildRoute<ModularRoute>(
          '/login',
          child: (BuildContext context, ModularArguments args) {
            return const LoginView();
          },
          transition: TransitionType.fadeIn,
        ),
        ChildRoute<ModularRoute>(
          '/register',
          child: (BuildContext context, ModularArguments args) {
            return const RegisterView();
          },
          transition: TransitionType.fadeIn,
        ),
        ModuleRoute<ModularRoute>(
          '/home',
          module: HomeModule(),
          transition: TransitionType.fadeIn,
          guards: <RouteGuard>[
            AuthGuard(),
          ],
        ),
      ];
}
