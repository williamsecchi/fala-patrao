import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/enum/type_user.dart';
import '../viewmodel/register_viewmodel.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final RegisterViewModel _viewModel = Modular.get<RegisterViewModel>();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ScaffoldMessenger(
        key: _viewModel.scaffoldMessengerKey,
        child: Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(16),
              child: Form(
                key: _viewModel.formKey,
                child: ListView(
                  children: <Widget>[
                    Text(
                      'Realize o seu cadastro',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Nome',
                      ),
                      controller: _viewModel.nameController,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'O nome não pode estar em branco!';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'E-mail',
                      ),
                      controller: _viewModel.emailController,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'O e-mail não pode estar em branco!';
                        }
                        if (!value.contains('@') || !value.contains('.')) {
                          return 'E-mail inválido!';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Senha',
                      ),
                      controller: _viewModel.passwordController,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'A senha não pode estar em branco!';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Confirme a senha',
                      ),
                      controller: _viewModel.passwordConfirmationController,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'A senha não pode estar em branco!';
                        }
                        if (value != _viewModel.passwordController.text) {
                          return 'As senhas não conferem!';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 10),
                    DropdownButtonFormField<TypeUser>(
                      decoration: const InputDecoration(
                        labelText: 'Você deseja ',
                      ),
                      value: _viewModel.typeUser,
                      items: _viewModel.typeUsers
                          .map((TypeUser value) => DropdownMenuItem<TypeUser>(
                                value: value,
                                child: Text(getTypeByName(value)),
                              ))
                          .toList(),
                      onChanged: (TypeUser? value) {
                        setState(() {
                          _viewModel.setTypeUser(value!);
                        });
                      },
                    ),
                    const SizedBox(height: 16),
                    ElevatedButton(
                      onPressed: () => _viewModel.register(),
                      child: const Text('Cadastrar'),
                    ),
                    ElevatedButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Voltar'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
