import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../viewmodel/login_viewmodel.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final LoginViewModel _viewModel = Modular.get<LoginViewModel>();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return ScaffoldMessenger(
      key: _viewModel.scaffoldMessengerKey,
      child: Scaffold(
        body: SafeArea(
          child: Form(
            key: _viewModel.formKey,
            child: ListView(
              padding: const EdgeInsets.all(16),
              children: <Widget>[
                SizedBox(height: size.height * .15),
                Text(
                  'Login',
                  style: Theme.of(context).textTheme.headline4,
                ),
                TextFormField(
                  controller: _viewModel.emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'O e-mail não pode ser vazio!';
                    }
                    if (!value.contains('@') || !value.contains('.')) {
                      return 'E-mail inválido!';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _viewModel.passwordController,
                  decoration: const InputDecoration(
                    labelText: 'Senha',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'A senha não pode estar em branco!';
                    }
                    return null;
                  },
                ),
                Container(
                  height: 60,
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      _viewModel.login(context);
                    },
                    child: const Text('Entrar'),
                  ),
                ),
                Container(
                  height: 60,
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      _viewModel.register(context);
                    },
                    child: const Text('Cadastro'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
