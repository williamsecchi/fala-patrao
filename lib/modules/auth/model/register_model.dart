import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/enum/type_user.dart';

class RegisterModel {

  GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController passwordConfirmationController =
      TextEditingController();

  TypeUser typeUser = TypeUser.normalUser;
}
