import 'package:flutter_modular/flutter_modular.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

import '../../core/guard/auth_guard.dart';
import '../auth/auth_module.dart';
import '../home/home_module.dart';
import 'view/splash_view.dart';
import 'viewmodel/splash_viewmodel.dart';

class SplashModule extends Module {
  @override
  List<Bind<Object>> get binds => <Bind<Object>>[
        Bind<SplashViewModel>(
          (Injector<dynamic> i) => SplashViewModel(
            i.get(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute<ModularRoute>(
          '/',
          child: (_, __) => const SplashView(),
        ),
        ModuleRoute<ModularRoute>(
          'auth/',
          module: AuthModule(),
          transition: TransitionType.fadeIn,
        ),
        ModuleRoute<ModularRoute>(
          'home/',
          module: HomeModule(),
          transition: TransitionType.fadeIn,
          guards: <RouteGuard>[
            AuthGuard(),
          ],
        ),
      ];
}
