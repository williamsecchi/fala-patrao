import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/interfaces/app_service.dart';
import '../../../core/interfaces/auth_interface.dart';
import '../../../core/services/AuthServiceImplementation.dart';

class SplashViewModel {
  final IAppService _appService;

  SplashViewModel(this._appService);

  void checkUserIsLoggedInAndNavigate() async {
    await Future<dynamic>.delayed(
      const Duration(seconds: 3),
      () async {
        final IAuthSerice authService =
            Modular.get<AuthServiceImplementation>();
        final bool isUserLoggedIn = await authService.isLogged();

        if (isUserLoggedIn) {
          final bool isTypeUserPresent = await _appService.getAndSetTypeUser(
            FirebaseAuth.instance.currentUser!.uid,
          );

          if (isTypeUserPresent) {
            Modular.to.pushReplacementNamed('home/');
          } else {
            Modular.to.pushReplacementNamed('auth/');
          }
        } else {
          Modular.to.pushReplacementNamed('auth/');
        }
      },
    );
  }
}
