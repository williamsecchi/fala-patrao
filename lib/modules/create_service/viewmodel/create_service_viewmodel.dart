import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../../../core/interfaces/app_service.dart';
import '../../../core/interfaces/auth_interface.dart';
import '../../../core/model/app_model.dart';
import '../../../core/model/prestador_servico/prestador_servico_model.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/create_service_model.dart';

class CreateServiceViewModel extends FirebaseOperationsImplementation {
  final CreateServiceModel _model = CreateServiceModel();

  final IAuthSerice _authService;

  final IAppService _appService;

  CreateServiceViewModel(
    this._authService,
    this._appService,
  );

  List<CategoryModel> get categories => _appService.getCategories();

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      _model.scaffoldMessengerKey;

  GlobalKey<FormState> get formKey => _model.formKey;

  TextEditingController get categoria => _model.categoria;

  TextEditingController get servicos => _model.servicos;

  TextEditingController get descricao => _model.descricao;

  TextEditingController get valorAproximado => _model.valorAproximado;

  TextEditingController get dateCreated => _model.dateCreated;

  TextEditingController get dateUpdated => _model.dateUpdated;

  Stream<PrestadorServicoModel> get prestadorServicoModelStream =>
      _model.prestadorServicoModelStream.stream;

  void init() async {
    User? user = await _authService.getUser();

    if (user != null) {
      final String uid = user.uid;
      final DatabaseReference databaseReference =
          FirebaseDatabase.instance.ref().child('prestador_servico').child(uid);

      Stream<DatabaseEvent> dbEvent = getDocument(databaseReference);

      dbEvent.listen(
        (DatabaseEvent event) {
          if (event.snapshot.value != null) {
            final Map<dynamic, dynamic> data =
                event.snapshot.value as Map<dynamic, dynamic>;

            final PrestadorServicoModel prestadorServicoModel =
                PrestadorServicoModel.fromJson(data);

            _model.prestadorServicoModelStream.sink.add(prestadorServicoModel);
          }

          if (event.snapshot.value == null) {
            _model.prestadorServicoModelStream.sink.addError(
              'Nenhum serviço encontrado. Por favor tente cadastrar algum serviço clicando no botão localizado no canto inferior direito.',
            );
          }
        },
      );
    }
  }

  void navigateToServiceCategory({
    required BuildContext context,
    required String categoryName,
  }) {
    Navigator.pushNamed(
      context,
      'serviceCategory?categoryName=$categoryName',
    );
  }

  void dispose() {
    _model.prestadorServicoModelStream.close();
  }

  void navigateToCreateService(BuildContext context) {
    Navigator.pushNamed(context, 'form');
  }
}
