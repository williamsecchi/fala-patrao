import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/interfaces/app_service.dart';
import '../../../core/interfaces/auth_interface.dart';
import '../../../core/model/app_model.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/form_creation_model.dart';

class FormCreationServiceViewModel extends FirebaseOperationsImplementation {
  final FormCreationModel formCreationServiceModel = FormCreationModel();

  final IAppService appService;

  final IAuthSerice authService;

  FormCreationServiceViewModel(
    this.appService,
    this.authService,
  );

  List<CategoryModel> get categories => appService.getCategories();

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      formCreationServiceModel.scaffoldMessengerKey;

  GlobalKey<FormState> get formKey => formCreationServiceModel.formKey;

  TextEditingController get categoria => formCreationServiceModel.categoria;

  TextEditingController get nomeServico => formCreationServiceModel.nomeServico;

  TextEditingController get descricao => formCreationServiceModel.descricao;

  TextEditingController get valorAproximado =>
      formCreationServiceModel.valorAproximado;

  void registerService() async {
    User? user = await authService.getUser();

    if (user != null) {
      Map<String, dynamic> registerMap = <String, dynamic>{
        'uid': user.uid,
        'categoria': categoria.text.trim().toLowerCase(),
        'nomeServico': nomeServico.text.trim().toLowerCase(),
        'nomePrestador': user.displayName,
        'urlFotoPrestador': user.photoURL,
        'valorAproximado': valorAproximado.text.trim().toUpperCase(),
        'descricao': descricao.text.trim().toLowerCase(),
        'dateCreated': DateTime.now().toLocal().toString(),
        'dateUpdated': '',
      };

      final DatabaseReference databaseReference = FirebaseDatabase.instance
          .ref()
          .child('servicos')
          .child(
            categoria.text.trim().toLowerCase(),
          )
          .child(user.uid)
          .child(
            DateTime.now().millisecondsSinceEpoch.toString(),
          );

      await setDocument(databaseReference, registerMap);

      scaffoldMessengerKey.currentState!.showSnackBar(
        snackBar(
          text: 'Serviço cadastrado com sucesso!',
          color: Colors.green,
        ),
      );

      nomeServico.clear();
      valorAproximado.clear();
      descricao.clear();

      return;
    }

    scaffoldMessengerKey.currentState!.showSnackBar(
      snackBar(
        text:
            'Não foi possível registrar o seu serviço, por favor tente novamente mais tarde.',
        color: Colors.red,
      ),
    );
  }

  SnackBar snackBar({
    required String text,
    required Color color,
  }) {
    return SnackBar(
      content: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: color,
      duration: const Duration(seconds: 5),
    );
  }
}
