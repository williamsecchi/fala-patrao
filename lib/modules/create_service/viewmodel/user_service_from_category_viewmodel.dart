import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../../../core/interfaces/auth_interface.dart';
import '../../../core/model/prestador_servico/servicos_oferecidos.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/user_services_from_category_model.dart';

class UserServiceFromCategoryViewModel
    extends FirebaseOperationsImplementation {
  final UserServiceFromCategoryModel _model = UserServiceFromCategoryModel();

  final IAuthSerice _authService;

  UserServiceFromCategoryViewModel(this._authService);

  StreamController<Servico> get servicos => _model.servicos;

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      _model.scaffoldMessengerKey;

  void init(String category) async {
    _model.servicos = StreamController<Servico>.broadcast();
    User? user = await _authService.getUser();

    if (user != null) {
      DatabaseReference ref = FirebaseDatabase.instance
          .ref()
          .child('servicos')
          .child(category.toLowerCase())
          .child(user.uid);
      Stream<DatabaseEvent> dbEvent = getDocument(ref);

      dbEvent.listen(
        (DatabaseEvent event) {
          if (event.snapshot.value != null) {
            final Map<dynamic, dynamic> data =
                event.snapshot.value as Map<dynamic, dynamic>;

            Servico? aux = Servico(
              servicoCategoria: <ServicoCategoria>[],
            );

            for (String element in data.keys) {
              ServicoCategoria services = ServicoCategoria.fromJson(
                <String, dynamic>{
                  'dataRegistro': element,
                  'servicos': data[element],
                },
              );

              aux.servicoCategoria?.add(services);
            }

            servicos.add(aux);
          }

          if (!servicos.isClosed) {
            if (event.snapshot.value == null) {
              servicos.sink.addError(
                  'Você ainda não possui serviços cadastrados nessa categoria, utilize o botão abaixo para cadastrar um novo serviço.');
            }
          }
        },
      );
    }
  }

  void dispose() {
    _model.servicos.close();
  }

  void navigateToCreateService(BuildContext context) {
    Navigator.pushNamed(context, 'form');
  }
}
