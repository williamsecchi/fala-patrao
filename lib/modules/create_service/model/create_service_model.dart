import 'dart:async';

import 'package:flutter/material.dart';

import '../../../core/model/prestador_servico/prestador_servico_model.dart';

class CreateServiceModel {
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  StreamController<PrestadorServicoModel> prestadorServicoModelStream =
      StreamController<PrestadorServicoModel>();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController categoria = TextEditingController();

  TextEditingController servicos = TextEditingController();

  TextEditingController descricao = TextEditingController();

  TextEditingController valorAproximado = TextEditingController();

  TextEditingController dateCreated = TextEditingController();

  TextEditingController dateUpdated = TextEditingController();
}
