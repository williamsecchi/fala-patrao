import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormCreationModel {
  // ref
  // child('categoria')
  // child('prestador')

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController categoria = TextEditingController();

  final TextEditingController nomeServico = TextEditingController();

  final TextEditingController descricao = TextEditingController();

  final TextEditingController valorAproximado = TextEditingController();
}
