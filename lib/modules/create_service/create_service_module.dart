import 'package:app/modules/create_service/view/form_creation_service.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:modular_interfaces/modular_interfaces.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'view/create_service_view.dart';
import 'view/user_services_from_category_view.dart';
import 'viewmodel/create_service_viewmodel.dart';
import 'viewmodel/form_creation_service_viewmodel.dart';
import 'viewmodel/user_service_from_category_viewmodel.dart';

class CreateServiceModule extends Module {
  @override
  final List<Bind<Object>> binds = <Bind<Object>>[
    Bind<CreateServiceViewModel>(
      (Injector<dynamic> i) => CreateServiceViewModel(
        i.get(),
        i.get(),
      ),
    ),
    Bind<FormCreationServiceViewModel>(
      (Injector<dynamic> i) => FormCreationServiceViewModel(
        i.get(),
        i.get(),
      ),
    ),
    Bind<UserServiceFromCategoryViewModel>(
      (Injector<dynamic> i) => UserServiceFromCategoryViewModel(
        i.get(),
      ),
    ),
  ];

  @override
  final List<ModularRoute> routes = <ModularRoute>[
    ChildRoute<ModularRoute>(
      '/',
      child: (_, __) => const CreateServiceView(),
      transition: TransitionType.rightToLeft,
    ),
    ChildRoute<ModularRoute>(
      '/form',
      child: (_, __) => const FormCreateNewServiceForCategoryView(),
      transition: TransitionType.rightToLeft,
    ),
    ChildRoute<ModularRoute>(
      '/serviceCategory',
      child: (BuildContext context, ModularArguments args) =>
          UserServicesFromCategoryView(
        categoryName: args.queryParams['categoryName']!,
      ),
      transition: TransitionType.rightToLeft,
    )
  ];
}
