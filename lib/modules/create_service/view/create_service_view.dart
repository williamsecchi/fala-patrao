import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/model/prestador_servico/prestador_servico_model.dart';
import '../viewmodel/create_service_viewmodel.dart';

class CreateServiceView extends StatefulWidget {
  const CreateServiceView({Key? key}) : super(key: key);

  @override
  State<CreateServiceView> createState() => _CreateServiceViewState();
}

class _CreateServiceViewState extends State<CreateServiceView> {
  final CreateServiceViewModel _viewModel =
      Modular.get<CreateServiceViewModel>();

  @override
  void initState() {
    super.initState();
    _viewModel.init();
  }

  @override
  void dispose() {
    _viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Seus serviços'),
      ),
      body: StreamBuilder<PrestadorServicoModel>(
        stream: _viewModel.prestadorServicoModelStream,
        initialData: null,
        builder: (BuildContext context,
            AsyncSnapshot<PrestadorServicoModel?> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: _viewModel.categories.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  margin: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color:
                          _viewModel.categories[index].color.withOpacity(0.7),
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: ListTile(
                    leading: Icon(
                      _viewModel.categories[index].iconData,
                      color: _viewModel.categories[index].color,
                    ),
                    title: Text(
                      _viewModel.categories[index].categoryName,
                    ),
                    onTap: () {
                      _viewModel.navigateToServiceCategory(
                        context: context,
                        categoryName: _viewModel.categories[index].categoryName,
                      );
                    },
                  ),
                );
              },
            );
          }
          if (snapshot.hasError) {
            return Text('Erro: ${snapshot.error}');
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _viewModel.navigateToCreateService(context);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
