import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/model/app_model.dart';
import '../viewmodel/form_creation_service_viewmodel.dart';

class FormCreateNewServiceForCategoryView extends StatefulWidget {
  const FormCreateNewServiceForCategoryView({Key? key}) : super(key: key);

  @override
  State<FormCreateNewServiceForCategoryView> createState() =>
      _FormCreateNewServiceForCategoryViewState();
}

class _FormCreateNewServiceForCategoryViewState
    extends State<FormCreateNewServiceForCategoryView> {
  final FormCreationServiceViewModel _viewModel =
      Modular.get<FormCreationServiceViewModel>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return ScaffoldMessenger(
      key: _viewModel.scaffoldMessengerKey,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Cadastrar serviço'),
        ),
        body: SafeArea(
          child: Form(
            key: _viewModel.formKey,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    'Por favor cadastre o seu serviço abaixo.',
                    style: Theme.of(context).textTheme.headline4,
                    textAlign: TextAlign.left,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.nomeServico,
                    decoration: const InputDecoration(
                      labelText: 'Serviço',
                    ),
                    keyboardType: TextInputType.name,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.descricao,
                    decoration: const InputDecoration(
                      labelText: 'Descrição do serviço',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.valorAproximado,
                    decoration: const InputDecoration(
                      labelText: 'Valor Aproximado em R\$',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      RealInputFormatter(moeda: true),
                    ],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: DropdownButtonFormField<CategoryModel>(
                    decoration: const InputDecoration(
                      labelText: 'Categoria do Serviço',
                    ),
                    items: _viewModel.categories.map((CategoryModel category) {
                      return DropdownMenuItem<CategoryModel>(
                        value: category,
                        child: buildDropdownMenuItem(category.categoryName),
                      );
                    }).toList(),
                    validator: (CategoryModel? value) {
                      if (value == null) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    onChanged: (CategoryModel? category) {
                      _viewModel.categoria.text = category!.categoryName;
                    },
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  width: double.infinity,
                  height: 60,
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      if (!_viewModel.formKey.currentState!.validate()) return;

                      _viewModel.registerService();
                    },
                    child: const Text('Cadastrar serviço'),
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> buildDropdownMenuItem(String item) {
    return DropdownMenuItem<String>(
      value: item,
      child: Text(item),
    );
  }
}
