import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/model/prestador_servico/servicos_oferecidos.dart';
import '../viewmodel/user_service_from_category_viewmodel.dart';

class UserServicesFromCategoryView extends StatefulWidget {
  const UserServicesFromCategoryView({
    Key? key,
    required this.categoryName,
  }) : super(key: key);

  final String categoryName;

  @override
  State<UserServicesFromCategoryView> createState() =>
      _UserServicesFromCategoryViewState();
}

class _UserServicesFromCategoryViewState
    extends State<UserServicesFromCategoryView> {
  final UserServiceFromCategoryViewModel _viewModel =
      Modular.get<UserServiceFromCategoryViewModel>();

  @override
  void initState() {
    super.initState();
    _viewModel.init(widget.categoryName);
  }

  @override
  void dispose() {
    _viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _viewModel.scaffoldMessengerKey,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Categoria ${fistLetterToUpperCase(
            widget.categoryName.toLowerCase(),
          )}'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _viewModel.navigateToCreateService(context);
          },
          child: const Icon(Icons.add),
        ),
        body: StreamBuilder<Servico>(
          initialData: null,
          stream: _viewModel.servicos.stream,
          builder: (BuildContext context, AsyncSnapshot<Servico> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data!.servicoCategoria?.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(
                      snapshot.data?.servicoCategoria?[index].servicos
                              ?.nomeServico ??
                          'N/A',
                    ),
                    subtitle: Text(
                      snapshot.data?.servicoCategoria?[index].servicos
                              ?.valorAproximado ??
                          'N/A',
                    ),
                  );
                },
              );
            }
            if (snapshot.hasError) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    snapshot.error.toString(),
                  ),
                ),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

  String fistLetterToUpperCase(String text) {
    return text[0].toUpperCase() + text.substring(1);
  }
}
