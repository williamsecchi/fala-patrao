import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../../../core/interfaces/auth_interface.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/proposal_data_model.dart';
import '../model/proposals_model.dart';

class ProposalsViewModel extends FirebaseOperationsImplementation {
  final ProposalsModel _model = ProposalsModel();

  final IAuthSerice authService;

  ProposalsViewModel({
    required this.authService,
  });

  void init() {
    _model.proposalsStreamController = StreamController<ProposalDataModel>();

    DatabaseReference ref = FirebaseDatabase.instance.ref().child('servicos');
    Stream<DatabaseEvent> dbEventStream = getDocument(ref);

    dbEventStream.listen(
      (DatabaseEvent event) {
        if (event.snapshot.value == null) {
          _model.proposalsStreamController.addError(
            'Não há registros de solicitações de contato',
          );
        }

        if (event.snapshot.value != null) {
          List<Service> _services = List<Service>.empty(growable: true);

          for (DataSnapshot element in event.snapshot.children) {
            for (DataSnapshot uidKey in element.children) {
              if (uidKey.key == FirebaseAuth.instance.currentUser!.uid) {
                for (DataSnapshot dateTimeKey in uidKey.children) {
                  for (DataSnapshot serviceKey in dateTimeKey.children) {
                    if (serviceKey.key == 'pessoafisicacontato') {
                      for (DataSnapshot serviceData in serviceKey.children) {
                        /*
                          debugPrint('LOG ? ###########');
                          debugPrint('LOG ? CATEGORY: ${element.key}');
                          debugPrint('LOG ? UID_SERVICE_PROVIDER: ${uidKey.key}');
                          debugPrint('LOG ? SERVICE_KEY: ${dateTimeKey.key}');
                          debugPrint(
                            'LOG ? UID_PESSOA_FISICA: ${serviceData.key}',
                          );
                          debugPrint(
                            'LOG ? NOME_PESSOA_FISICA: ${(serviceData.value as Map<dynamic, dynamic>)['name']}',
                          );
                         */

                        _services.add(
                          Service(
                            serviceCategory: element.key!,
                            uid: uidKey.key!,
                            key: dateTimeKey.key!,
                            uidPessoaFisica: serviceData.key!,
                            nomePessoaFisica: (serviceData.value
                                as Map<dynamic, dynamic>)['name'],
                            statusPessoaFisica: (serviceData.value
                                as Map<dynamic, dynamic>)['status'],
                            serviceName: (dateTimeKey.value
                                as Map<dynamic, dynamic>)['nomeServico'],
                          ),
                        );
                      }
                    }
                  }
                }
              }
            }
          }

          ProposalDataModel proposalDataModel = ProposalDataModel(
            service: _services,
          );

          // TODO: TRATAR ERRO  Unhandled Exception: Bad state: Cannot add event after closing ao alterar status da solicitação de contato
          if (_model.proposalsStreamController.isClosed) {
            _model.proposalsStreamController =
                StreamController<ProposalDataModel>();
          }
          _model.proposalsStreamController.sink.add(
            proposalDataModel,
          );
        }
      },
    );
  }

  void dispose() {
    _model.proposalsStreamController.close();
  }

  Stream<ProposalDataModel> get proposalsStreamController =>
      _model.proposalsStreamController.stream;

  Future<void> _updateProposalStatus({
    required String category,
    required String uid,
    required String registerKey,
    required String uidPessoaFisica,
    required String status,
  }) async {
    DatabaseReference ref = FirebaseDatabase.instance
        .ref()
        .child('servicos')
        .child(category)
        .child(uid)
        .child(registerKey)
        .child('pessoafisicacontato')
        .child(uidPessoaFisica);
    await updateDocument(
      ref,
      <String, dynamic>{
        'status': status,
      },
    );
  }

  Future<void> approveContactRequest({
    required String category,
    required String registerKey,
    required String uidPessoaFisica,
    required String status,
  }) async {
    final User? user = await authService.getUser();

    if (user != null) {
      debugPrint('LOG ? - ##########');
      debugPrint('LOG ? - $category');
      debugPrint('LOG ? - $registerKey');
      debugPrint('LOG ? - $uidPessoaFisica');
      debugPrint('LOG ? - $status');

      _updateProposalStatus(
        category: category,
        uid: user.uid,
        registerKey: registerKey,
        uidPessoaFisica: uidPessoaFisica,
        status: status,
      );
    } else {
      throw Exception('Usuário não autenticado - tratar erro');
    }
  }
}
