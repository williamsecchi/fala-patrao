class ProposalDataModel {
  ProposalDataModel({this.service});

  final List<Service>? service;

  @override
  String toString() {
    return 'ProposalDataModel(service: $service)';
  }
}

class Service {
  Service({
    required this.serviceCategory,
    required this.uid,
    required this.key,
    required this.nomePessoaFisica,
    required this.statusPessoaFisica,
    required this.uidPessoaFisica,
    required this.serviceName,
  });

  final String serviceCategory;
  final String uid;
  final String key;
  final String uidPessoaFisica;
  final String nomePessoaFisica;
  final String statusPessoaFisica;
  final String serviceName;

  @override
  String toString() {
    return 'Service(serviceCategory: $serviceCategory, uid: $uid, key: $key, uidPessoaFisica: $uidPessoaFisica, nomePessoaFisica: $nomePessoaFisica, statusPessoaFisica: $statusPessoaFisica, serviceName: $serviceName)';
  }
}
