import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/enum/contact_status.dart';
import '../model/proposal_data_model.dart';
import '../viewmodel/proposals_viewmodel.dart';

class ProposalsView extends StatefulWidget {
  const ProposalsView({Key? key}) : super(key: key);

  @override
  State<ProposalsView> createState() => _ProposalsViewState();
}

class _ProposalsViewState extends State<ProposalsView> {
  final ProposalsViewModel _viewModel = Modular.get<ProposalsViewModel>();

  @override
  void initState() {
    super.initState();
    _viewModel.init();
  }

  @override
  void dispose() {
    _viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Solitações de Contato'),
        centerTitle: false,
      ),
      body: StreamBuilder<ProposalDataModel>(
        initialData: null,
        stream: _viewModel.proposalsStreamController,
        builder: (
          BuildContext context,
          AsyncSnapshot<ProposalDataModel> snapshot,
        ) {
          if (snapshot.hasError) {
            return Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(
                  snapshot.error.toString(),
                  textAlign: TextAlign.center,
                ),
              ),
            );
          }

          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.service!.length,
              itemBuilder: (
                BuildContext context,
                int index,
              ) {
                return ListTile(
                  title: Text(
                    'Cliente: ${firstLetterToUpperCase(snapshot.data!.service![index].nomePessoaFisica)}',
                  ),
                  subtitle: Text(
                    '${firstLetterToUpperCase(snapshot.data!.service![index].serviceCategory)} - ${firstLetterToUpperCase(snapshot.data!.service![index].serviceName)}',
                  ),
                  trailing: InkWell(
                    onTap: () {
                      ContactStatus status = getStatusFromText(
                        snapshot.data!.service![index].statusPessoaFisica,
                      );

                      debugPrint('LOG ? Status: $status');

                      switch (status) {
                        case ContactStatus.requestContact:
                          {
                            // nunca irá cair aqui
                            break;
                          }
                        case ContactStatus.pending:
                          {
                            debugPrint('LOG ? - pending');
                            _showDialog(
                              title: 'Pendende',
                              body: <Widget>[
                                const Text('Aprovar ou recusar contato'),
                                const Text('Essa ação não poderá ser desfeita'),
                              ],
                              buttonText: 'Aprovar',
                              function: () {
                                _viewModel.approveContactRequest(
                                  category: snapshot
                                      .data!.service![index].serviceCategory,
                                  registerKey:
                                      snapshot.data!.service![index].key,
                                  uidPessoaFisica: snapshot
                                      .data!.service![index].uidPessoaFisica,
                                  status: 'aceito',
                                );
                                Navigator.pop(context);
                              },
                            );
                            break;
                          }
                        case ContactStatus.accepted:
                          {
                            debugPrint('LOG ? - accepted');
                            _showDialog(
                              title: 'Aceito',
                              body: <Widget>[
                                const Text('Você já aceitou esse contato'),
                                const Text('Essa ação não poderá ser desfeita'),
                              ],
                              showButton: false,
                              buttonText: '',
                              function: () {},
                            );
                            break;
                          }
                        case ContactStatus.rejected:
                          {
                            debugPrint('LOG ? - rejected');
                            _showDialog(
                              title: 'Recusado',
                              body: <Widget>[
                                const Text('Você recusou esse contato'),
                                const Text(
                                    'Você ainda pode aprovar o contato clicando abaixo!'),
                              ],
                              buttonText: 'Aprovar',
                              function: () {
                                _viewModel.approveContactRequest(
                                  category: snapshot
                                      .data!.service![index].serviceCategory,
                                  registerKey:
                                      snapshot.data!.service![index].key,
                                  uidPessoaFisica: snapshot
                                      .data!.service![index].uidPessoaFisica,
                                  status: 'aceito',
                                );
                                Navigator.pop(context);
                              },
                            );
                            break;
                          }
                      }
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('Status'),
                        Text(
                          snapshot.data!.service![index].statusPessoaFisica,
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  String firstLetterToUpperCase(String text) {
    return text[0].toUpperCase() + text.substring(1);
  }

  Future<void> _showDialog({
    required String title,
    required List<Widget> body,
    required String buttonText,
    bool showButton = true,
    required VoidCallback function,
  }) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            title,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: body,
            ),
          ),
          actions: <Widget>[
            if (showButton)
              TextButton(
                child: Text(buttonText),
                onPressed: () {
                  function.call();
                },
              ),
            TextButton(
              child: const Text('Cancelar'),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }
}
