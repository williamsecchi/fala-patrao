import '../../core/services/AuthServiceImplementation.dart';
import 'view/proposals_view.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:modular_interfaces/modular_interfaces.dart';
import 'viewmodel/proposals_viewmodel.dart';

class ProposalsModule extends Module {
  @override
  List<Bind<Object>> get binds => <Bind<Object>>[
        Bind<ProposalsViewModel>(
          (Injector<dynamic> i) => ProposalsViewModel(
            authService: i.get<AuthServiceImplementation>(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute<ModularRoute>(
          '/',
          child: (_, __) => const ProposalsView(),
          transition: TransitionType.rightToLeft,
        ),
      ];
}
