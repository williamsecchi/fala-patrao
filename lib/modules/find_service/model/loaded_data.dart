import '../../../core/model/prestador_servico/servicos_oferecidos.dart';

class LoadData {
  final String uid;
  final List<Servico> servicos;
  bool isExpanded;

  LoadData({
    required this.uid,
    required this.servicos,
    this.isExpanded = false,
  });

  factory LoadData.fromJson(Map<String, dynamic> json) {
    if (json['servicos'] != null) {
      final List<Servico> servicos = List<Servico>.empty(growable: true);

      for (String key in json['servicos'].keys) {
        final Map<dynamic, dynamic> aux =
            json['servicos'][key] as Map<dynamic, dynamic>;

        aux['key'] = key;

        servicos.add(
          Servico.fromJsonDynamic(aux),
        );
      }

      return LoadData(
        uid: json['uid'],
        servicos: servicos,
      );
    } else {
      return LoadData(
        uid: json['uid'],
        servicos: <Servico>[],
      );
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'servicos': servicos,
      };

  @override
  String toString() {
    return 'LoadData{uid: $uid, servicos: $servicos}';
  }
}
