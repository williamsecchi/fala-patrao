import 'package:modular_interfaces/modular_interfaces.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'view/find_service_view.dart';
import 'viewmodel/find_service_viewmodel.dart';

class FindServiceModule extends Module {
  @override
  final List<Bind<Object>> binds = <Bind<Object>>[
    Bind<FindServiceViewModel>(
      (Injector<dynamic> i) => FindServiceViewModel(),
    ),
  ];

  @override
  final List<ModularRoute> routes = <ModularRoute>[
    ChildRoute<ModularRoute>(
      '/:category',
      child: (_, ModularArguments args) => FindServiceView(
        category: args.params['category'],
      ),
    ),
  ];
}
