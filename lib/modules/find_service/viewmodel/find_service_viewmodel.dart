import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/find_service_model.dart';
import '../model/loaded_data.dart';

class FindServiceViewModel extends FirebaseOperationsImplementation {
  final FindServiceModel _model = FindServiceModel();

  StreamController<List<LoadData>> get services => _model.services;

  void init(String category) {
    _model.services = StreamController<List<LoadData>>();

    DatabaseReference ref =
        FirebaseDatabase.instance.ref().child('servicos').child(category);

    Stream<DatabaseEvent> dbEvent = getDocument(ref);

    dbEvent.listen(
      (DatabaseEvent event) {
        if (event.snapshot.value != null) {
          final Map<dynamic, dynamic> data =
              event.snapshot.value as Map<dynamic, dynamic>;

          List<LoadData> aux = List<LoadData>.empty(growable: true);

          for (String element in data.keys) {
            LoadData services = LoadData.fromJson(
              <String, dynamic>{
                'uid': element,
                'servicos': data[element],
              },
            );

            aux.add(services);
          }

          _model.services.sink.add(aux);
        }

        if (event.snapshot.value == null) {
          _model.services.addError('Nenhum serviço encontrado');
        }
      },
    );
  }

  void setDataIsExpanded(
      int index, bool isExpanded, List<LoadData> streamAux) async {
    streamAux[index].isExpanded = !isExpanded;
    _model.services.sink.add(streamAux);

    for (int i = 0; i < streamAux.length; i++) {
      if (streamAux[i].isExpanded && i != index) {
        streamAux[i].isExpanded = false;
        _model.services.sink.add(streamAux);
      }
    }
    /*
      _model.services.stream.single.then(
        (List<LoadData> value) {
          for (int i = 0; i < value.length; i++) {
            if (i == index) {
              value[i].isExpanded = isExpanded;
            } else {
              value[i].isExpanded = false;
            }
          }
        },
      );
     */
  }

  void sendContactRequest({
    required String category,
    required String uidPrestador,
    required String idServico,
    required Map<String, dynamic> map,
  }) async {
    DatabaseReference ref = FirebaseDatabase.instance
        .ref()
        .child('servicos')
        .child(category)
        .child(uidPrestador)
        .child(idServico)
        .child('pessoafisicacontato')
        .child(
          FirebaseAuth.instance.currentUser!.uid,
        );
    await setDocument(ref, map);
  }

  void dispose() {
    _model.services.close();
  }

  FutureOr<String?> getPhoneFromServiceProvider({
    required String uid,
  }) async {
    DatabaseReference ref =
        FirebaseDatabase.instance.ref().child('prestador_servico').child(uid);

    Stream<DatabaseEvent> result = getDocument(
      ref,
    );

    String? phone;
    result.listen((DatabaseEvent event) {
      if (event.snapshot.value != null) {
        final Map<dynamic, dynamic> data =
            event.snapshot.value as Map<dynamic, dynamic>;

        phone = data['telefone'];
      }
    });

    if (phone != null) {
      return phone;
    }

    await Future<dynamic>.delayed(const Duration(seconds: 1));

    if (phone != null) {
      return phone;
    }

    await Future<dynamic>.delayed(const Duration(seconds: 1));

    if (phone != null) {
      return phone;
    }

    return null;
  }
}
