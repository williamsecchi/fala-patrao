import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/model/prestador_servico/servicos_oferecidos.dart';
import '../../../core/enum/contact_status.dart';
import '../model/loaded_data.dart';
import '../viewmodel/find_service_viewmodel.dart';

class FindServiceView extends StatefulWidget {
  const FindServiceView({
    Key? key,
    required this.category,
  }) : super(key: key);

  final String category;

  @override
  State<FindServiceView> createState() => _FindServiceViewState();
}

class _FindServiceViewState extends State<FindServiceView> {
  final FindServiceViewModel _viewModel = Modular.get<FindServiceViewModel>();

  @override
  void initState() {
    super.initState();
    _viewModel.init(
      widget.category.toLowerCase(),
    );
  }

  @override
  void dispose() {
    _viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category),
      ),
      body: StreamBuilder<List<LoadData>>(
        stream: _viewModel.services.stream,
        builder:
            (BuildContext context, AsyncSnapshot<List<LoadData>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                snapshot.error.toString(),
              ),
            );
          }
          if (snapshot.hasData) {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: size.width * 0.05,
                vertical: size.height * 0.03,
              ),
              child: ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (BuildContext context, int listIndex) {
                  return Container(
                    margin: EdgeInsets.only(
                      bottom: size.height * 0.025,
                    ),
                    child: ExpansionPanelList(
                      expansionCallback: (int index, bool isExpanded) {
                        _viewModel.setDataIsExpanded(
                          listIndex,
                          isExpanded,
                          snapshot.data!,
                        );
                      },
                      children: <ExpansionPanel>[
                        ExpansionPanel(
                          headerBuilder:
                              (BuildContext context, bool isExpanded) {
                            return Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10,
                                  ),
                                  child: CircleAvatar(
                                    backgroundColor:
                                        Colors.grey.withOpacity(0.3),
                                    child: const Icon(
                                      Icons.person,
                                    ),
                                  ),
                                ),
                                Text(
                                  firstLetterToUpperCase(
                                        snapshot
                                            .data?[listIndex]
                                            .servicos
                                            .first
                                            .servicoCategoria
                                            ?.first
                                            .servicos
                                            ?.nomePrestador,
                                      ) ??
                                      '',
                                ),
                              ],
                            );
                          },
                          isExpanded:
                              snapshot.data?[listIndex].isExpanded ?? false,
                          canTapOnHeader: true,
                          body: Column(
                            children: List<Widget>.generate(
                              snapshot.data?[listIndex].servicos.length ?? 0,
                              (int index2) {
                                Color color = Colors.black26;
                                ContactStatus status =
                                    ContactStatus.requestContact;
                                if (snapshot
                                        .data?[listIndex]
                                        .servicos[index2]
                                        .servicoCategoria
                                        ?.first
                                        .servicos
                                        ?.pessoaFisicaContato
                                        ?.map((PessoaFisicaContato e) => e.uid)
                                        .contains(FirebaseAuth
                                            .instance.currentUser!.uid) ??
                                    false) {
                                  PessoaFisicaContato? pessoaFisicaContato =
                                      snapshot
                                          .data?[listIndex]
                                          .servicos[index2]
                                          .servicoCategoria
                                          ?.first
                                          .servicos
                                          ?.pessoaFisicaContato
                                          ?.firstWhere(
                                    (PessoaFisicaContato e) =>
                                        e.uid ==
                                        FirebaseAuth.instance.currentUser!.uid,
                                  );

                                  if (pessoaFisicaContato?.status ==
                                      'pendente') {
                                    color = Colors.orange;
                                    status = ContactStatus.pending;
                                  } else if (pessoaFisicaContato?.status ==
                                      'aceito') {
                                    color = Colors.green;
                                    status = ContactStatus.accepted;
                                  } else if (pessoaFisicaContato?.status ==
                                      'recusado') {
                                    color = Colors.red;
                                    status = ContactStatus.rejected;
                                  }
                                }
                                return ListTile(
                                  //TODO: ADICIONAR TELA DE MAIS INFORMAÇÕES
                                  onTap: () async {
                                    switch (status) {
                                      case ContactStatus.requestContact:
                                        {
                                          Map<String, dynamic> map =
                                              <String, dynamic>{};

                                          map['name'] = FirebaseAuth.instance
                                              .currentUser?.displayName;
                                          map['status'] = 'pendente';
                                          //TODO(WILLIAM): A MENSAGEM DEVE SER DIGITADA EM UM DIALOG
                                          map['msg'] =
                                              'mensagem enviada pelo usuário ao prestador de serviço';

                                          String? category = snapshot
                                              .data?[listIndex]
                                              .servicos[index2]
                                              .servicoCategoria
                                              ?.first
                                              .servicos
                                              ?.categoria;
                                          String? uidPrestador = snapshot
                                              .data?[listIndex]
                                              .servicos[index2]
                                              .servicoCategoria
                                              ?.first
                                              .servicos
                                              ?.uid;

                                          String? idServico = snapshot
                                              .data?[listIndex]
                                              .servicos[index2]
                                              .servicoCategoria
                                              ?.first
                                              .dataRegistro;

                                          if (category != null &&
                                              uidPrestador != null &&
                                              idServico != null) {
                                            _viewModel.sendContactRequest(
                                              category: category,
                                              uidPrestador: uidPrestador,
                                              idServico: idServico,
                                              map: map,
                                            );
                                          }
                                          break;
                                        }
                                      case ContactStatus.pending:
                                        {
                                          showDialog(
                                            context: context,
                                            builder: (_) {
                                              return AlertDialog(
                                                title: const Text(
                                                    'Aguardando resposta'),
                                                content: const Text(
                                                    'Aguarde a resposta do prestador de serviço'),
                                                actions: <Widget>[
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Ok'),
                                                  ),
                                                ],
                                              );
                                            },
                                          );

                                          break;
                                        }
                                      case ContactStatus.accepted:
                                        {
                                          String? uid = snapshot
                                              .data?[listIndex]
                                              .servicos[index2]
                                              .servicoCategoria
                                              ?.first
                                              .servicos
                                              ?.uid;

                                          String? phoneNumber;
                                          if (uid != null) {
                                            phoneNumber = await _viewModel
                                                .getPhoneFromServiceProvider(
                                              uid: uid,
                                            );

                                            phoneNumber ??= await _viewModel
                                                .getPhoneFromServiceProvider(
                                              uid: uid,
                                            );

                                            phoneNumber ??= await _viewModel
                                                .getPhoneFromServiceProvider(
                                              uid: uid,
                                            );
                                          }

                                          showDialog(
                                            context: context,
                                            builder: (_) {
                                              return AlertDialog(
                                                title: const Text(
                                                  'Contato aceito',
                                                ),
                                                content: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    const Text(
                                                      'O prestador de serviço aceitou seu contato',
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    if (phoneNumber == null)
                                                      const Text(
                                                          'Não foi possível obter o número de telefone do prestador de serviço'),
                                                    if (phoneNumber != null)
                                                      Text(
                                                        'Entre em contato pelo número: $phoneNumber',
                                                      ),
                                                  ],
                                                ),
                                                actions: <Widget>[
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Ok'),
                                                  ),
                                                ],
                                              );
                                            },
                                          );
                                          break;
                                        }
                                      case ContactStatus.rejected:
                                        {
                                          showDialog(
                                            context: context,
                                            builder: (_) {
                                              return AlertDialog(
                                                title: const Text(
                                                    'Contato recusado'),
                                                content: const Text(
                                                  'O prestador de serviço recusou seu contato',
                                                ),
                                                actions: <Widget>[
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Ok'),
                                                  ),
                                                ],
                                              );
                                            },
                                          );
                                          break;
                                        }
                                    }
                                  },
                                  title: Text(
                                    'Servico: ${firstLetterToUpperCase(
                                      snapshot
                                          .data?[listIndex]
                                          .servicos[index2]
                                          .servicoCategoria
                                          ?.first
                                          .servicos
                                          ?.nomeServico,
                                    )}',
                                  ),
                                  subtitle: Text(
                                    'Valor Aproximado: ${firstLetterToUpperCase(
                                      snapshot
                                          .data?[listIndex]
                                          .servicos[index2]
                                          .servicoCategoria
                                          ?.first
                                          .servicos
                                          ?.valorAproximado,
                                    )}',
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  style: ListTileStyle.list,
                                  visualDensity: VisualDensity.comfortable,
                                  trailing: FractionallySizedBox(
                                    widthFactor: 0.3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.chat_rounded,
                                          color: color,
                                        ),
                                        Text(
                                          contactStatusToString(status),
                                          style: const TextStyle(
                                            fontSize: 10,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  String? firstLetterToUpperCase(String? text) {
    if (text == null) return '';
    return text[0].toUpperCase() + text.substring(1);
  }
}
