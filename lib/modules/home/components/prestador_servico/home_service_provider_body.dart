import 'package:flutter/material.dart';

import '../../../../core/model/prestador_servico/prestador_servico_model.dart';
import '../../viewmodel/home_viewmodel.dart';
import '../card_widget.dart';

class HomeServiceProviderUser extends StatefulWidget {
  const HomeServiceProviderUser({
    Key? key,
    required this.viewModel,
  }) : super(key: key);

  final HomeViewModel viewModel;

  @override
  State<HomeServiceProviderUser> createState() =>
      _HomeServiceProviderUserState();
}

class _HomeServiceProviderUserState extends State<HomeServiceProviderUser> {
  @override
  void initState() {
    super.initState();
    widget.viewModel.initPrestadorServico();
  }

  @override
  void dispose() {
    widget.viewModel.disposePrestadorServico();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return StreamBuilder<PrestadorServicoModel>(
      stream: widget.viewModel.prestadorServicoModelStream.stream,
      initialData: null,
      builder: (BuildContext context,
          AsyncSnapshot<PrestadorServicoModel> snapshot) {
        if (snapshot.hasData) {
          return Body(
            snapshot: snapshot,
          );
        } else if (snapshot.hasError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  snapshot.error.toString(),
                  style: Theme.of(context).textTheme.bodyMedium,
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 60,
                width: size.width * .8,
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () =>
                      widget.viewModel.navigateToFormPrestadorServico(context),
                  child: const Text('Preencher informações'),
                ),
              ),
            ],
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class Body extends StatefulWidget {
  Body({
    Key? key,
    required this.snapshot,
  }) : super(key: key);

  AsyncSnapshot<PrestadorServicoModel> snapshot;

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints boxConstraints) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  left: 20,
                  right: 40,
                ),
                child: Text(
                  'Olá ${widget.snapshot.data!.nome}, fique a vontade para cadastrar os seus serviços e aguarde as propostas.',
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              GridView.count(
                shrinkWrap: true,
                primary: false,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                children: <Widget>[
                  CardWidget(
                    icon: Icons.miscellaneous_services_rounded,
                    text: 'Serviços',
                    backgroundColor: const Color(0xFF1F41F2),
                    onTap: () => Navigator.pushNamed(context, 'createService/'),
                  ),
                  CardWidget(
                    icon: Icons.wallet_rounded,
                    text: 'Propostas',
                    backgroundColor: const Color(0xFF6E29E6),
                    onTap: () => Navigator.pushNamed(context, 'proposals/'),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
