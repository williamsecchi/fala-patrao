import 'package:flutter/material.dart';

import '../../../../core/model/pessoa_fisica/pessoa_fisica_model.dart';
import '../../viewmodel/home_viewmodel.dart';
import '../card_widget.dart';

class HomeNormalUser extends StatefulWidget {
  const HomeNormalUser({
    Key? key,
    required this.viewModel,
  }) : super(key: key);

  final HomeViewModel viewModel;

  @override
  State<HomeNormalUser> createState() => _HomeNormalUserState();
}

class _HomeNormalUserState extends State<HomeNormalUser> {
  @override
  void initState() {
    super.initState();
    widget.viewModel.initPessoaFisica();
  }

  @override
  void dispose() {
    widget.viewModel.disposePessoaFisica();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return StreamBuilder<PessoaFisicaModel>(
      stream: widget.viewModel.pessoaFisicaModelStream.stream,
      initialData: null,
      builder:
          (BuildContext context, AsyncSnapshot<PessoaFisicaModel> snapshot) {
        if (snapshot.hasData) {
          return Body(
            snapshot: snapshot,
            viewModel: widget.viewModel,
          );
        } else if (snapshot.hasError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  snapshot.error.toString(),
                  style: Theme.of(context).textTheme.bodyMedium,
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 60,
                width: size.width * .8,
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () =>
                      widget.viewModel.navigateToFormPessoaFisica(context),
                  child: const Text('Preencher informações'),
                ),
              ),
            ],
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class Body extends StatefulWidget {
  Body({
    Key? key,
    required this.snapshot,
    required this.viewModel,
  }) : super(key: key);

  AsyncSnapshot<PessoaFisicaModel> snapshot;
  final HomeViewModel viewModel;

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints boxConstraints) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  left: 20,
                  right: 40,
                ),
                child: Text(
                  'Olá ${widget.snapshot.data!.nome}, fique a vontade para explorar e conhecer os serviços do app.',
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 20,
                ),
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 1,
                  ),
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.viewModel.appService.getCategories().length,
                  itemBuilder: (BuildContext context, int index) {
                    return CardWidget(
                      icon: widget.viewModel.appService
                          .getCategories()[index]
                          .iconData,
                      text: widget.viewModel.appService
                          .getCategories()[index]
                          .categoryName,
                      backgroundColor: widget.viewModel.appService
                          .getCategories()[index]
                          .color,
                      onTap: () {
                        widget.viewModel.searchForServices(
                          context,
                          widget.viewModel.appService
                              .getCategories()[index]
                              .categoryName,
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
