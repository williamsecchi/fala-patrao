import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../viewmodel/home_normal_user_viewmodel.dart';

class FormCompleteRegistrationPessoaFisica extends StatefulWidget {
  const FormCompleteRegistrationPessoaFisica({Key? key}) : super(key: key);

  @override
  State<FormCompleteRegistrationPessoaFisica> createState() =>
      _FormCompleteRegistrationPessoaFisicaState();
}

class _FormCompleteRegistrationPessoaFisicaState
    extends State<FormCompleteRegistrationPessoaFisica> {
  final HomeNormalUserViewModel _viewModel =
      Modular.get<HomeNormalUserViewModel>();

  @override
  void initState() {
    super.initState();
    _viewModel.estadoController.text = _viewModel.estados.firstWhere(
      (String e) => e.contains('RS'),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return ScaffoldMessenger(
      key: _viewModel.scaffoldMessengerKey,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Completar cadastro'),
        ),
        body: SafeArea(
          child: Form(
            key: _viewModel.formKey,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    'Para que possamos te ajudar, precisamos que você complete seu cadastro.',
                    style: Theme.of(context).textTheme.headline4,
                    textAlign: TextAlign.left,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.nomeController,
                    decoration: const InputDecoration(
                      labelText: 'Nome',
                    ),
                    keyboardType: TextInputType.name,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.sobrenomeController,
                    decoration: const InputDecoration(
                      labelText: 'Sobrenome',
                    ),
                    keyboardType: TextInputType.name,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.cpfController,
                    decoration: const InputDecoration(
                      labelText: 'CPF',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      CpfInputFormatter(),
                    ],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      if (!CPFValidator.isValid(value)) {
                        return 'CPF inválido';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.telefoneController,
                    decoration: const InputDecoration(
                      labelText: 'Telefone',
                    ),
                    keyboardType: TextInputType.phone,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      TelefoneInputFormatter(),
                    ],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      (value.length < 14) ? 'Telefone inválido' : null;
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.enderecoController,
                    decoration: const InputDecoration(
                      labelText: 'Endereço',
                    ),
                    keyboardType: TextInputType.streetAddress,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // TODO(William): Implementar utilizando dropdown
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.cidadeController,
                    decoration: const InputDecoration(
                      labelText: 'Cidade',
                    ),
                    keyboardType: TextInputType.streetAddress,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: DropdownButtonFormField(
                    decoration: const InputDecoration(
                      labelText: 'Estado',
                    ),
                    items: _viewModel.estados
                        .map(
                          (String e) => buildDropdownMenuItem(e),
                        )
                        .toList(),
                    onChanged: (String? estado) {
                      _viewModel.estadoController.text = estado!;
                    },
                    value: _viewModel.estadoController.text,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.cepController,
                    decoration: const InputDecoration(
                      labelText: 'CEP',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      CepInputFormatter(),
                    ],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: _viewModel.dataNascimentoController,
                    decoration: const InputDecoration(
                      labelText: 'Data de nascimento',
                    ),
                    keyboardType: TextInputType.datetime,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      DataInputFormatter(),
                    ],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo obrigatório';
                      }

                      int year;
                      int month;
                      int day;

                      try {
                        year = int.parse(value.split('/')[2]);
                        month = int.parse(value.split('/')[1]);
                        day = int.parse(value.split('/')[0]);
                      } catch (e) {
                        return 'Data inválida';
                      }

                      DateTime dataNacimento = DateTime(year, month, day);

                      if (dataNacimento.isAfter(
                        DateTime.now(),
                      )) {
                        return 'Data inválida';
                      }
                      if (dataNacimento.isBefore(
                        DateTime(1900),
                      )) {
                        return 'Data inválida';
                      }
                      if (dataNacimento.isAfter(
                        DateTime.now().subtract(
                          const Duration(days: 365 * 18),
                        ),
                      )) {
                        return 'Você deve ser maior de idade';
                      }
                      if (dataNacimento.isBefore(
                        DateTime.now().subtract(
                          const Duration(days: 365 * 100),
                        ),
                      )) {
                        return 'Data inválida';
                      }

                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  width: double.infinity,
                  height: 60,
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      if (!_viewModel.formKey.currentState!.validate()) return;

                      _viewModel.completeRegistration();
                    },
                    child: const Text('Completar cadastro'),
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> buildDropdownMenuItem(String item) {
    return DropdownMenuItem<String>(
      value: item,
      child: Text(item),
    );
  }
}
