import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../viewmodel/home_viewmodel.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({
    Key? key,
    required HomeViewModel viewModel,
  })  : _viewModel = viewModel,
        super(key: key);

  final HomeViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.white,
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              FirebaseAuth.instance.currentUser?.displayName ?? 'N/A',
            ),
            accountEmail: Text(
              FirebaseAuth.instance.currentUser?.email ?? 'N/A',
            ),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.white,
              backgroundImage: CachedNetworkImageProvider(
                FirebaseAuth.instance.currentUser?.photoURL ??
                    'https://png.pngtree.com/png-clipart/20190520/original/pngtree-vector-add-user-icon-png-image_4102544.jpg',
              ),
            ),
          ),
          ListTile(
            title: const Text('Home'),
            leading: const Icon(Icons.home),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed('/home/');
            },
          ),
          ListTile(
            title: const Text('Sair'),
            leading: const Icon(
              Icons.logout_rounded,
            ),
            onTap: () => _viewModel.logout(context),
          ),
        ],
      ),
    );
  }
}
