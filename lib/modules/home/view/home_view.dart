import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/enum/type_user.dart';
import '../components/home/home_drawer.dart';
import '../components/pessoa_fisica/home_normal_user_body.dart';
import '../components/prestador_servico/home_service_provider_body.dart';
import '../viewmodel/home_viewmodel.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final HomeViewModel _viewModel = Modular.get<HomeViewModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Início'),
      ),
      drawer: HomeDrawer(viewModel: _viewModel),
      body: _viewModel.appService.getTypeUser() == TypeUser.normalUser
          ? HomeNormalUser(
              viewModel: _viewModel,
            )
          : HomeServiceProviderUser(
              viewModel: _viewModel,
            ),
    );
  }
}
