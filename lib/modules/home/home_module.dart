import 'package:app/modules/home/viewmodel/home_prestador_servico_viewmodel.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

import '../create_service/create_service_module.dart';
import '../find_service/find_service_module.dart';
import '../proposals/proposals_module.dart';
import 'components/pessoa_fisica/form_complete_registration_pessoa_fisica.dart';
import 'components/prestador_servico/form_complete_registration_prestador_servico.dart';
import 'view/home_view.dart';
import 'viewmodel/home_normal_user_viewmodel.dart';
import 'viewmodel/home_viewmodel.dart';

class HomeModule extends Module {
  @override
  List<Bind<Object>> get binds => <Bind<Object>>[
        Bind<HomeViewModel>(
          (Injector<dynamic> i) => HomeViewModel(
            i.get(),
          ),
        ),
        Bind<HomeNormalUserViewModel>(
          (Injector<dynamic> i) => HomeNormalUserViewModel(
            i.get(),
            i.get(),
          ),
        ),
        Bind<HomePrestadorServicoViewModel>(
          (Injector<dynamic> i) => HomePrestadorServicoViewModel(
            i.get(),
            i.get(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute<ModularRoute>(
          '/',
          child: (_, __) => const HomeView(),
          transition: TransitionType.fadeIn,
        ),
        ChildRoute<ModularRoute>(
          '/formPessoaFisica',
          child: (_, __) => const FormCompleteRegistrationPessoaFisica(),
          transition: TransitionType.rightToLeft,
        ),
        ChildRoute<ModularRoute>(
          '/formPrestadorServico',
          child: (_, __) {
            return const FormCompleteRegistrationPrestadorServico();
          },
        ),
        ModuleRoute<ModularRoute>(
          '/proposals',
          module: ProposalsModule(),
        ),
        ModuleRoute<ModularRoute>(
          '/createService',
          module: CreateServiceModule(),
          transition: TransitionType.rightToLeft,
        ),
        ModuleRoute<ModularRoute>(
          '/searchForService',
          module: FindServiceModule(),
          transition: TransitionType.rightToLeft,
        ),
      ];
}
