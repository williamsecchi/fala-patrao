import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../../../core/interfaces/auth_interface.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/pessoa_fisica/home_normal_user_model.dart';
import 'home_viewmodel.dart';

class HomeNormalUserViewModel extends FirebaseOperationsImplementation {
  final HomeNormalUserModel _model = HomeNormalUserModel();

  final IAuthSerice _authService;

  final HomeViewModel _homeViewModel;

  HomeNormalUserViewModel(this._authService, this._homeViewModel);

  /*
        *  Dados do formulário de finalização de cadastro da pessoa física
   */

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      _model.scaffoldMessengerKey;

  GlobalKey<FormState> get formKey => _model.formKey;

  List<String> get estados => _homeViewModel.estados;

  TextEditingController get nomeController => _model.nomeController;

  TextEditingController get sobrenomeController => _model.sobrenomeController;

  TextEditingController get cpfController => _model.cpfController;

  TextEditingController get telefoneController => _model.telefoneController;

  TextEditingController get enderecoController => _model.enderecoController;

  TextEditingController get cidadeController => _model.cidadeController;

  TextEditingController get estadoController => _model.estadoController;

  TextEditingController get cepController => _model.cepController;

  TextEditingController get dataNascimentoController =>
      _model.dataNascimentoController;

  TextEditingController get sexoController => _model.sexoController;

  Future<bool> completeRegistration() async {
    User? user = await _authService.getUser();

    if (user != null) {
      DatabaseReference reference = FirebaseDatabase.instance
          .ref()
          .child('pessoa_fisica')
          .child(user.uid);

      Map<String, dynamic> map = <String, dynamic>{
        'uid': user.uid,
        'nome': nomeController.text.trim(),
        'sobrenome': sobrenomeController.text.trim(),
        'email': user.email?.trim(),
        'cpf': cpfController.text.trim(),
        'telefone': telefoneController.text.trim(),
        'endereco': enderecoController.text.trim(),
        'cidade': cidadeController.text.trim(),
        'estado': estadoController.text.trim(),
        'cep': cepController.text.trim(),
        'dataNascimento': dataNascimentoController.text.trim(),
        'sexo': sexoController.text.trim(),
        'dateCreated': user.metadata.creationTime?.toIso8601String(),
        'dateUpdated': DateTime.now().toString(),
        'registrationComplete': true,
      };

      setDocument(reference, map);

      return true;
    }

    return false;
  }
}
