import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import '../../../core/enum/type_user.dart';
import '../../../core/interfaces/app_service.dart';
import '../../../core/services/FirebaseOperationsImplementation.dart';
import '../model/home_model.dart';
import '../../../core/model/pessoa_fisica/pessoa_fisica_model.dart';
import '../../../core/model/prestador_servico/prestador_servico_model.dart';

class HomeViewModel extends FirebaseOperationsImplementation {
  final HomeModel _model = HomeModel();

  final IAppService appService;

  HomeViewModel(this.appService);

  void logout(BuildContext context) {
    FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, '/auth/login');
  }

  TypeUser? getTypeUser() {
    return appService.getTypeUser();
  }

  void getPessoaFisicaInfo() {
    DatabaseReference _pessoaFisicaRef = FirebaseDatabase.instance
        .ref()
        .child('pessoa_fisica')
        .child(FirebaseAuth.instance.currentUser!.uid);

    Stream<DatabaseEvent> dbEvent = getDocument(_pessoaFisicaRef);

    dbEvent.listen(
      (DatabaseEvent event) {
        if (event.snapshot.value != null) {
          final Map<dynamic, dynamic> pessoaFisicaMap =
              event.snapshot.value as Map<dynamic, dynamic>;

          _model.pessoaFisicaModel.sink.add(
            PessoaFisicaModel.fromJson(
              pessoaFisicaMap,
            ),
          );
        }
        if (event.snapshot.value == null) {
          _model.pessoaFisicaModel.sink.addError(
            'Para que você possa utilizar o aplicativo e visualizar os prestadores de serviço da sua região, é necessário que você preencha mais alguns dados.',
          );
        }
      },
    );
  }

  void getPrestadorServicoInfo() {
    final DatabaseReference _prestadorServicoRef =
        FirebaseDatabase.instance.ref().child('prestador_servico').child(
              FirebaseAuth.instance.currentUser!.uid,
            );

    Stream<DatabaseEvent> dbEvent = getDocument(_prestadorServicoRef);

    dbEvent.listen(
      (DatabaseEvent event) {
        if (event.snapshot.value != null) {
          final Map<dynamic, dynamic> prestadorServicoMap =
              event.snapshot.value as Map<dynamic, dynamic>;

          _model.prestadorServicoModel.sink.add(
            PrestadorServicoModel.fromJson(
              prestadorServicoMap,
            ),
          );
        }
        if (event.snapshot.value == null) {
          _model.prestadorServicoModel.sink.addError(
            'Para que você possa utilizar o aplicativo e tornar seus serviços visíveis para os clientes'
            ' precisamos preencher mais alguns dados.',
          );
        }
      },
    );
  }

  StreamController<PessoaFisicaModel> get pessoaFisicaModelStream =>
      _model.pessoaFisicaModel;

  StreamController<PrestadorServicoModel> get prestadorServicoModelStream =>
      _model.prestadorServicoModel;

  initPessoaFisica() {
    _model.pessoaFisicaModel = StreamController<PessoaFisicaModel>();
    getPessoaFisicaInfo();
  }

  disposePessoaFisica() {
    _model.pessoaFisicaModel.close();
  }

  initPrestadorServico() {
    _model.prestadorServicoModel = StreamController<PrestadorServicoModel>();
    getPrestadorServicoInfo();
  }

  disposePrestadorServico() {
    _model.prestadorServicoModel.close();
  }

  void navigateToFormPessoaFisica(BuildContext context) {
    Navigator.pushNamed(context, 'formPessoaFisica');
  }

  void navigateToFormPrestadorServico(BuildContext context) {
    Navigator.pushNamed(context, 'formPrestadorServico');
  }

  List<String> get estados => _model.estados;

  void searchForServices(BuildContext context, String category) {
    Navigator.pushNamed(context, 'searchForService/$category');
  }
}
