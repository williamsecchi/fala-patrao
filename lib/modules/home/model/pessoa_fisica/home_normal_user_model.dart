import 'package:flutter/material.dart';

class HomeNormalUserModel {
  /*
        *  Dados do formulário de finalização de cadastro da pessoa física
   */

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController nomeController = TextEditingController();

  TextEditingController sobrenomeController = TextEditingController();

  TextEditingController cpfController = TextEditingController();

  TextEditingController telefoneController = TextEditingController();

  TextEditingController enderecoController = TextEditingController();

  TextEditingController cidadeController = TextEditingController();

  TextEditingController estadoController = TextEditingController();

  TextEditingController cepController = TextEditingController();

  TextEditingController dataNascimentoController = TextEditingController();

  TextEditingController sexoController = TextEditingController();

}
