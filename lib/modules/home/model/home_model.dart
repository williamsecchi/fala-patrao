import 'dart:async';

import 'package:app/core/model/prestador_servico/prestador_servico_model.dart';

import '../../../core/model/pessoa_fisica/pessoa_fisica_model.dart';

class HomeModel {
  StreamController<PessoaFisicaModel> pessoaFisicaModel =
      StreamController<PessoaFisicaModel>();

  StreamController<PrestadorServicoModel> prestadorServicoModel =
      StreamController<PrestadorServicoModel>();

  List<String> estados = <String>[
    'Acre - AC',
    'Alagoas - AL',
    'Amapá - AP',
    'Amazonas - AM',
    'Bahia - BA',
    'Ceará - CE',
    'Distrito Federal - DF',
    'Espírito Santo - ES',
    'Goiás - GO',
    'Maranhão - MA',
    'Mato Grosso - MT',
    'Mato Grosso do Sul - MS',
    'Minas Gerais - MG',
    'Pará - PA',
    'Paraíba - PB',
    'Paraná - PR',
    'Pernambuco - PE',
    'Piauí - PI',
    'Rio de Janeiro - RJ',
    'Rio Grande do Norte - RN',
    'Rio Grande do Sul - RS',
    'Rondônia - RO',
    'Roraima - RR',
    'Santa Catarina - SC',
    'São Paulo - SP',
    'Sergipe - SE',
    'Tocantins - TO',
  ];

  List<String> categorias = <String>[
    'Casa',
    'Oficina',
    'Transporte',
    'Pet',
    'Construção',
    'Carpintaria',
    'Outros'
  ];
}
