import 'package:flutter/material.dart';

class HomePrestadorServicoModel {
  /*
        *  Dados do formulário de finalização de cadastro do prestador de servico
   */

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
  GlobalKey<ScaffoldMessengerState>();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController nomeController = TextEditingController();

  TextEditingController sobrenomeController = TextEditingController();

  TextEditingController cpfController = TextEditingController();

  TextEditingController telefoneController = TextEditingController();

  TextEditingController enderecoController = TextEditingController();

  TextEditingController cidadeController = TextEditingController();

  TextEditingController estadoController = TextEditingController();

  TextEditingController cepController = TextEditingController();

  TextEditingController dataNascimentoController = TextEditingController();

  TextEditingController sexoController = TextEditingController();



}
