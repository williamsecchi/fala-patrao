import 'package:flutter_modular/flutter_modular.dart';

import 'core/interfaces/app_service.dart';
import 'core/interfaces/auth_interface.dart';
import 'core/services/AppServiceImplementation.dart';
import 'core/services/AuthServiceImplementation.dart';
import 'modules/splash/splash_module.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

class AppModule extends Module {
  @override
  List<Bind<Object>> get binds => <Bind<Object>>[
        Bind<IAuthSerice>(
          (Injector<dynamic> i) => AuthServiceImplementation(),
        ),
        Bind<IAppService>(
          (Injector<dynamic> i) => AppServiceImplementation(),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ModuleRoute<ModularRoute>(
          '/',
          module: SplashModule(),
          transition: TransitionType.fadeIn,
        ),
      ];
}
