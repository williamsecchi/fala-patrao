import 'dart:async';

import 'package:flutter_modular/flutter_modular.dart';

import '../../../core/interfaces/auth_interface.dart';

class AuthGuard extends RouteGuard {
  AuthGuard() : super(redirectTo: '../auth/');

  final IAuthSerice _authSerice = Modular.get<IAuthSerice>();

  @override
  FutureOr<bool> canActivate(String path, ParallelRoute route) async {
    if (await _authSerice.isLogged() && await _authSerice.isEmailVerified()) {
      return true;
    }
    return false;
  }
}
