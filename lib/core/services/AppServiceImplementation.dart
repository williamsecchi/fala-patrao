import 'dart:async';

import 'package:firebase_database/firebase_database.dart';

import '../enum/type_user.dart';
import '../interfaces/app_service.dart';
import '../model/app_model.dart';
import 'FirebaseOperationsImplementation.dart';

class AppServiceImplementation extends FirebaseOperationsImplementation
    implements IAppService {
  final AppModel _model = AppModel();

  @override
  void setTypeUser(TypeUser typeUser) {
    _model.typeUser = typeUser;
  }

  @override
  TypeUser? getTypeUser() {
    return _model.typeUser;
  }

  @override
  FutureOr<bool> getAndSetTypeUser(String uid) async {
    final DatabaseReference userRef =
        FirebaseDatabase.instance.ref().child('usuarios/$uid');
    DatabaseEvent dbEvent = await userRef.once(DatabaseEventType.value);

    if (dbEvent.snapshot.value != null) {
      final Map<dynamic, dynamic> userMap =
          dbEvent.snapshot.value as Map<dynamic, dynamic>;

      setTypeUser(TypeUser.values[userMap['type']]);

      return true;
    }
    return false;
  }

  @override
  List<CategoryModel> getCategories() {
    return _model.categories;
  }
}
