import '../interfaces/firebase_operations.dart';
import 'package:firebase_database/firebase_database.dart';

class FirebaseOperationsImplementation implements IFirebaseOperation {

  @override
  Stream<DatabaseEvent> getDocument(DatabaseReference reference) {
    return reference.onValue;
  }

  @override
  Future<void> setDocument(DatabaseReference reference, Map<String, dynamic> data) {
    return reference.set(data);
  }

  @override
  Future<void> updateDocument(DatabaseReference reference, Map<String, dynamic> data) {
    return reference.update(data);
  }

  @override
  void deleteDocument(DatabaseReference reference) {
    reference.remove();
  }
}
