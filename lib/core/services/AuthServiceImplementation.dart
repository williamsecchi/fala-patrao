import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';

import '../enum/type_success_error.dart';
import '../interfaces/auth_interface.dart';
import '../model/success_error.dart';

class AuthServiceImplementation implements IAuthSerice {
  @override
  FutureOr<bool> isLogged() {
    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      return Future<bool>.value(true);
    }

    return Future<bool>.value(false);
  }

  @override
  FutureOr<bool> login(
    String email,
    String password,
  ) {
    return FirebaseAuth.instance
        .signInWithEmailAndPassword(
          email: email,
          password: password,
        )
        .then(
          (UserCredential value) => Future<bool>.value(true),
        )
        .catchError(
          (error) => Future<bool>.value(false),
        );
  }

  @override
  FutureOr<bool> logout() {
    return FirebaseAuth.instance
        .signOut()
        .then(
          (value) => Future<bool>.value(true),
        )
        .catchError(
          (error) => Future<bool>.value(false),
        );
  }

  @override
  FutureOr<SuccessError> register(String email, String password) {
    return FirebaseAuth.instance
        .createUserWithEmailAndPassword(
          email: email,
          password: password,
        )
        .then(
          (UserCredential value) => SuccessError(
            TypeSuccessError.success,
            value,
          ),
        )
        .catchError(
          (error) => SuccessError(
            TypeSuccessError.error,
            error,
          ),
        );
  }

  @override
  FutureOr<User?> getUser() {
    return Future<User?>.value(
      FirebaseAuth.instance.currentUser,
    );
  }

  @override
  FutureOr<bool> isEmailVerified() {
    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      return Future<bool>.value(user.emailVerified);
    }

    return Future<bool>.value(false);
  }

  @override
  void resentEmailVerification() {
    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      user.sendEmailVerification();
    }
  }

  @override
  FutureOr<void> getAndSetTypeUser() {
    // TODO: implement getAndSetTypeUser
    throw UnimplementedError();
  }
}
