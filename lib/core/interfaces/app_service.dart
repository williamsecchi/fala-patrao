import 'dart:async';

import '../enum/type_user.dart';
import '../model/app_model.dart';

abstract class IAppService {
  void setTypeUser(TypeUser typeUser);

  TypeUser? getTypeUser();

  FutureOr<bool> getAndSetTypeUser(String uid);

  List<CategoryModel> getCategories();
}
