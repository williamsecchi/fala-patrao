import 'package:firebase_database/firebase_database.dart';

abstract class IFirebaseOperation {
  Future<void> setDocument(DatabaseReference reference, Map<String, dynamic> data);

  Future<void> updateDocument(DatabaseReference reference, Map<String, dynamic> data);

  Stream<DatabaseEvent> getDocument(DatabaseReference reference);

  void deleteDocument(DatabaseReference reference);
}
