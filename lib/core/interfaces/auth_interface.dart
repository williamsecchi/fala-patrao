import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';

import '../model/success_error.dart';

abstract class IAuthSerice {
  FutureOr<bool> login(String email, String password);

  FutureOr<bool> logout();

  FutureOr<SuccessError> register(String email, String password);

  FutureOr<bool> isLogged();

  FutureOr<bool> isEmailVerified();

  FutureOr<User?> getUser();

  void resentEmailVerification();
}
