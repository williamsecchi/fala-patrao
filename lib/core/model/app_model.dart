import 'package:flutter/material.dart';

import '../enum/type_user.dart';

class AppModel {
  TypeUser? typeUser;

  List<CategoryModel> categories = <CategoryModel>[
    CategoryModel(
      categoryName: 'Casa',
      iconData: Icons.home_rounded,
      color: const Color(0xFFDD1FF2),
    ),
    CategoryModel(
      categoryName: 'Oficina',
      iconData: Icons.car_repair_rounded,
      color: const Color(0xFF1F41F2),
    ),
    CategoryModel(
      categoryName: 'Transporte',
      iconData: Icons.emoji_transportation_rounded,
      color: Colors.blue,
    ),
    CategoryModel(
      categoryName: 'Pet',
      iconData: Icons.pets_rounded,
      color: const Color(0xFF6E29E6),
    ),
    CategoryModel(
      categoryName:'Construção',
      iconData:Icons.construction_rounded,
      color: const Color(0xFFe6ac84),
    ),
    CategoryModel(
      categoryName:'Carpintaria',
      iconData:Icons.carpenter_rounded,
      color: const Color(0xFF855e6e),
    ),
    CategoryModel(
      categoryName: 'Outros',
      iconData: Icons.lightbulb_rounded,
      color: const Color(0xFF3a352f),
    ),
  ];
}

class CategoryModel {
  final String categoryName;
  final IconData iconData;
  final Color color;

  CategoryModel({
    required this.categoryName,
    required this.iconData,
    required this.color,
  });
}
