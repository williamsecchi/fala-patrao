class PrestadorServicoModel {
  String? uid;
  String? nome;
  String? sobrenome;
  String? email;
  String? cpf;
  String? telefone;
  String? endereco;
  String? cidade;
  String? estado;
  String? cep;
  String? dataNascimento;
  String? sexo;
  String? dateCreated;
  String? dateUpdated;
  bool? registrationComplete;

  PrestadorServicoModel({
    this.uid,
    this.nome,
    this.sobrenome,
    this.email,
    this.cpf,
    this.telefone,
    this.endereco,
    this.cidade,
    this.estado,
    this.cep,
    this.dataNascimento,
    this.sexo,
    this.dateCreated,
    this.dateUpdated,
    this.registrationComplete,
  });

  PrestadorServicoModel.fromJson(Map<dynamic, dynamic> json)
      : uid = json['uid'],
        nome = json['nome'],
        sobrenome = json['sobrenome'],
        email = json['email'],
        cpf = json['cpf'],
        telefone = json['telefone'],
        endereco = json['endereco'],
        cidade = json['cidade'],
        estado = json['estado'],
        cep = json['cep'],
        dataNascimento = json['dataNascimento'],
        sexo = json['sexo'],
        dateCreated = json['dateCreated'],
        dateUpdated = json['dateUpdated'],
        registrationComplete = json['registrationComplete'];

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'nome': nome,
        'sobrenome': sobrenome,
        'email': email,
        'cpf': cpf,
        'telefone': telefone,
        'endereco': endereco,
        'cidade': cidade,
        'estado': estado,
        'cep': cep,
        'dataNascimento': dataNascimento,
        'sexo': sexo,
        'dateCreated': dateCreated,
        'dateUpdated': dateUpdated,
        'registrationComplete': registrationComplete,
      };

  @override
  String toString() {
    return 'PrestadorServicoModel{uid: $uid, nome: $nome, sobrenome: $sobrenome, email: $email, cpf: $cpf, telefone: $telefone, endereco: $endereco, cidade: $cidade, estado: $estado, cep: $cep, dataNascimento: $dataNascimento, sexo: $sexo, dateCreated: $dateCreated, dateUpdated: $dateUpdated, registrationComplete: $registrationComplete}';
  }
}
