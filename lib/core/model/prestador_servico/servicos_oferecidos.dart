import 'package:flutter/material.dart';

class Servico {
  List<ServicoCategoria>? servicoCategoria;

  Servico({
    required this.servicoCategoria,
  });

  factory Servico.fromJson(Map<String, dynamic> json) {
    return Servico(
      servicoCategoria: json['servicoCategoria'] != null
          ? (json['servicoCategoria'] as List)
              .map((i) => ServicoCategoria.fromJson(i))
              .toList()
          : null,
    );
  }

  factory Servico.fromJsonDynamic(Map<dynamic, dynamic> json) {
    final Map<String, dynamic> map = <String, dynamic>{
      'dataRegistro': json['key'],
      'servicos': json,
    };

    return Servico(
      servicoCategoria: <ServicoCategoria>[
        ServicoCategoria.fromJson(map),
      ],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (servicoCategoria != null) {
      data['servicoCategoria'] =
          servicoCategoria!.map((ServicoCategoria v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return 'Servico{servicoCategoria: $servicoCategoria}';
  }
}

class ServicoCategoria {
  String? dataRegistro;
  ServicosOferecidos? servicos;

  ServicoCategoria({
    this.dataRegistro,
    this.servicos,
  });

  factory ServicoCategoria.fromJson(Map<String, dynamic> json) {
    return ServicoCategoria(
      dataRegistro: json['dataRegistro'],
      servicos: ServicosOferecidos.fromJson(
        json['servicos'],
      ),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'dataRegistro': dataRegistro,
        'servicos': servicos,
      };

  @override
  String toString() {
    return 'ServicoCategoria{dataRegistro: $dataRegistro, servicos: $servicos}';
  }
}

class ServicosOferecidos {
  String? uid;
  String? nomePrestador;
  String? urlFotoPrestador;
  String? categoria;
  String? nomeServico;
  String? descricao;
  String? valorAproximado;
  String? dateCreated;
  String? dateUpdated;
  List<PessoaFisicaContato>? pessoaFisicaContato;

  ServicosOferecidos({
    this.uid,
    this.categoria,
    this.nomeServico,
    this.nomePrestador,
    this.urlFotoPrestador,
    this.valorAproximado,
    this.descricao,
    this.dateCreated,
    this.dateUpdated,
    this.pessoaFisicaContato,
  });

  factory ServicosOferecidos.fromJson(Map<dynamic, dynamic> json) {
    List<PessoaFisicaContato> pessoaFisicaContato =
        List<PessoaFisicaContato>.empty(growable: true);

    if (json['pessoafisicacontato'] != null) {
      for (String key in json['pessoafisicacontato'].keys) {
        final Map<dynamic, dynamic> aux =
            json['pessoafisicacontato'][key] as Map<dynamic, dynamic>;
        aux['uid'] = key;
        pessoaFisicaContato.add(
          PessoaFisicaContato.fromJson(aux),
        );
      }
    }

    return ServicosOferecidos(
      uid: json['uid'],
      categoria: json['categoria'],
      nomeServico: json['nomeServico'],
      nomePrestador: json['nomePrestador'],
      urlFotoPrestador: json['urlFotoPrestador'],
      valorAproximado: json['valorAproximado'],
      descricao: json['descricao'],
      dateCreated: json['dateCreated'],
      dateUpdated: json['dateUpdated'],
      pessoaFisicaContato: pessoaFisicaContato,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'categoria': categoria,
        'nomeServico': nomeServico,
        'nomePrestador': nomePrestador,
        'urlFotoPrestador': urlFotoPrestador,
        'valorAproximado': valorAproximado,
        'descricao': descricao,
        'dateCreated': dateCreated,
        'dateUpdated': dateUpdated,
        'pessoaFisicaContato': pessoaFisicaContato,
      };

  @override
  String toString() {
    return 'ServicosOferecidos{uid: $uid, nomePrestador: $nomePrestador, urlFotoPrestador: $urlFotoPrestador, categoria: $categoria, nomeServico: $nomeServico, descricao: $descricao, valorAproximado: $valorAproximado, dateCreated: $dateCreated, dateUpdated: $dateUpdated, pessoaFisicaContato: $pessoaFisicaContato}';
  }
}

class PessoaFisicaContato {
  String? name;
  String? uid;
  String? status;
  String? msg;

  PessoaFisicaContato({
    this.name,
    this.uid,
    this.status,
    this.msg,
  });

  PessoaFisicaContato.fromJson(Map<dynamic, dynamic> json)
      : name = json['name'],
        uid = json['uid'],
        status = json['status'],
        msg = json['msg'];

  Map<String, dynamic> toJson() => <String, dynamic>{
        'name': name,
        'uid': uid,
        'status': status,
        'msg': msg,
      };

  @override
  String toString() {
    return 'PessoaFisicaContato{name: $name, uid: $uid, status: $status, msg: $msg}';
  }
}
