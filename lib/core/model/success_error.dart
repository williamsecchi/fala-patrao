import '../enum/type_success_error.dart';

class SuccessError {
  final TypeSuccessError type;
  final dynamic value;

  SuccessError(
    this.type,
    this.value,
  );

  bool get isSuccess => type == TypeSuccessError.success;
}
