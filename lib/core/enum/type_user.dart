enum TypeUser {
  normalUser,
  serviceProvider,
}

String getTypeByName(TypeUser typeUser) {
  switch (typeUser) {
    case TypeUser.normalUser:
      return 'Buscar Prestadores de Serviço';
    case TypeUser.serviceProvider:
      return 'Divulgar e Prestar Serviços';
  }
}
