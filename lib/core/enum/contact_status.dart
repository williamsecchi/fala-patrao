enum ContactStatus {
  requestContact,
  pending,
  accepted,
  rejected,
}

String contactStatusToString(ContactStatus status) {
  switch (status) {
    case ContactStatus.requestContact:
      return 'Solicitar Contato';
    case ContactStatus.pending:
      return 'Pendente';
    case ContactStatus.accepted:
      return 'Aceito';
    case ContactStatus.rejected:
      return 'Recusado';
    default:
      return 'Solicitar Contato';
  }
}

ContactStatus getStatusFromText(String status) {
  switch (status) {
    case 'pendente':
      return ContactStatus.pending;
    case 'aceito':
      return ContactStatus.accepted;
    case 'recusado':
      return ContactStatus.rejected;
    default:
      return ContactStatus.requestContact;
  }
}
